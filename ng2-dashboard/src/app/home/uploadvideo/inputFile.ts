import {Component, Output, EventEmitter, ViewChild, ElementRef, Input} from '@angular/core';

@Component({
  selector: 'input-file',
  templateUrl: './file.tpl.html'
})
export class InputFile {
  @Input() accept: string;
  @Output() onFileSelect: EventEmitter<File[]> = new EventEmitter();

  @ViewChild('inputFile') nativeInputFile: ElementRef;

  private _files: File[];

  get fileCount(): number { return this._files && this._files.length || 0; }

  onNativeInputFileSelect($event) {
    console.log("onNativeInputFileSelect in InputFile");
    console.log($event);
    this._files = $event.srcElement.files;
    this.onFileSelect.emit(this._files);
  }

  selectFile() {
    console.log("selectFile in InputFile");
    console.log(this.nativeInputFile.nativeElement);
    this.nativeInputFile.nativeElement.click();
  }
}
