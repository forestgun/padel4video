import { Injectable } from '@angular/core';

import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate  } from '@angular/router';



@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(
    private router:Router
  ) { }

  canActivate ( ){
    // devolver true o false
    console.log('AuthGuard#canActivate called');
    return true;

  }


}
