
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponentComponent } from './home-component/home-component.component';
import { UploadvideoComponent } from './uploadvideo/uploadvideo.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PedidoComponent } from './pedido/pedido.component';


const routes: Routes = [
  {
    path: 'home', component: HomeComponentComponent, children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {path: 'uploadvideo', component: UploadvideoComponent},
      {path: 'profile', component: ProfileComponent},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'pedido/:id', component: PedidoComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule { }
