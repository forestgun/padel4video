import { Component, OnInit, EventEmitter, AfterViewInit, ViewChild } from '@angular/core';
import { Router , ActivatedRoute, Params} from '@angular/router';
import { NgForm } from '@angular/forms';
import { ParseobjectService } from '../../services/parseobject.service';

//import { UploadOutput, UploadInput, UploadFile, humanizeBytes } from 'ngx-uploader';

//import {  NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';

//import { FileUploader } from 'ng2-file-upload';
//import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';

import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';

import { UUID } from 'angular2-uuid';

import {  MdProgressBarModule } from '@angular/material';


const URL = 'https://not_exit.herokuapp.com/api/';


import * as Parse from 'parse';
declare var Stripe: any;

@Component({
  selector: 'app-uploadvideo',
  templateUrl: './uploadvideo.component.html',
  styleUrls: ['./uploadvideo.component.css']
})

export class UploadvideoComponent implements OnInit, AfterViewInit {

 // @ViewChild(ChildDirective) child: ChildDirective;
 // @ViewChild("tpl") tpl: TemplateRef<any>;

 pedido: any;
 id: any;

  color: string ;
  mode: string ;
  isvalid1: boolean;
  isvalid2: boolean;
  issend: boolean;

  ext_thumbnail: string;
  domain_cloudfront: string;
  prefix_cloudfront: string;
  cloudfront: string;
  prefix_S3: string;
  usercurrent: any;
  pointerusercurrent: any;
  tokencheckout: any;
  videos_uploaded: Array<any> = []; // pointers a los videos en BD PARSE
  files_uploaded: Array<any>;       
  pago_saved: any;
  pago: any;
  
  importe: number;

  formData: any;


  file_upload: any;


  

  tipospedidos: any;
  estadospedidos: any;

  tipopedidoseleccionado: any;
  observaciones: string;



  //ng2-file-upload
  URL: string = '';
  public uploader: FileUploader;


  constructor(private parseobject: ParseobjectService,
              private router: Router,
              private route: ActivatedRoute
  ) {

    this.uploader = new FileUploader({url: URL});

    this.id = this.route.snapshot.params['id'];

  }

  ngAfterViewInit() {
    console.log('UploadvideoComponent --> ngAfterViewInit() ');

  }


  ngOnInit() {
    console.log('UploadvideoComponent --> ngOnInit() ');

    //let id = +this.route.snapshot.params['id'];
    this.pedido = this.parseobject.getPedido(this.id);

    this.usercurrent = Parse.User.current();

    this.color = 'accent';
    this.mode = 'determinate';



    this.ext_thumbnail = '-00001.png';
    this.prefix_S3 = 'analysis_source/';
    this.prefix_cloudfront = '' ;
    this.domain_cloudfront = 'https://d2bewn7qfy36b2.cloudfront.net/';
    this.cloudfront = this.domain_cloudfront + this.prefix_cloudfront;
    this.files_uploaded = [];  // array con nombre de files subidos (incluyendo el UUID)

    this.importe = 1990; // IMPORTE A PAGAR en centimos


    this.isvalid1 = true;   // Disable boton Video1
    this.isvalid2 = true;   // disable boton Video2
    this.file_upload = null;
    this.issend = false;    // ACTIVA EL SPINNER  Y DISABLE EN CONTEN DEL FORMULARIO
    this.pago_saved = {};  //una vez grabado el pago en parse se almacena aqui el pointer
    this.pago = {};        // el pago grabado en parse 
    this.pedido = {};      // el pedido grabado en parse


    this.tipospedidos = this.parseobject.tipospedidos;
    this.estadospedidos = this.parseobject.estadospedidos;

    this.tipopedidoseleccionado = null;
    this.observaciones = '';

    this.tokencheckout = null;

    this.pointerusercurrent = this.usercurrent.toPointer();


    //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
      file.method = 'PUT';
    };
    //overide the onCompleteItem property of the uploader so we are 
    //able to deal with the server response.
    this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
        console.log("ImageUpload:uploaded:", item, status, response);
    };


  }

// validar habilitar boton y mostrar progresion
  file1ChangeEvent($event) {
    console.log("file1ChangeEvent --> ");
    console.log($event);
    console.log($event.srcElement.files);
    if ($event.srcElement.files.length === 1){
      this.isvalid1 = false;
    } else {
      this.isvalid1 = true;
    }

  }

/*
  file2ChangeEvent($event) {
    console.log("file2ChangeEvent --> ");
    console.log($event);
    console.log($event.srcElement.files);
    if ($event.srcElement.files.length === 1){
      this.isvalid2 = false;
    } else {
      this.isvalid2 = true;
    }

  }
*/

  buttomUploadChange() {
    console.log('buttomUploadChange --> ');
    console.log(this.issend);
    this.issend = !this.issend;
    console.log(this.issend);
  }


// FORMULARIO NUEVO PEDIDO
  onSubmit(formpedido: NgForm) {
    console.log("onSubmit(formpedido)");
    console.log(formpedido);
    //console.log(formpedido.value);
    //console.log(formpedido.value.observaciones);
    //console.log(formpedido.value.tipopedido);
    this.formData = formpedido;

    this.observaciones = formpedido.value.observaciones;

    /*
    for ( let tipo of this.tipospedidos) {
      //console.log(tipo);
      if (tipo.id == formpedido.value.tipopedido) {
        this.tipopedidoseleccionado = tipo;
        //console.log(this.tipopedidoseleccionado);
      }
    }
    */

    this.buttomUploadChange(); // cambiamos estado del boton

    /*
    this.pagarPedido().then( (result) => {
      console.log("result de pagarPedido()");
      console.log(result);
      this.tokencheckout = result;
      // TODO SUBIR EL VIEDO y SALVAR LOS DATOS DE PAGO Y PEDIDO
      // SAVE PAGO
      this.saveModelPago();
      

      //this.upload(formpedido);
      this.ng2UploadS3().then((result) => {
        console.log("result de ng2UploadS3()");
        console.log(result);
        // TODO LANZAR PARSE.CLOUD QUE REALIZE EL CARGO SOBRE EL TOKEN DEL PAGO (SINO SE HACE NO SE COBRA)
        this.parseobject.chargePayStripe(this.tokencheckout.id,
                                         this.usercurrent.get('username'),
                                         this.importe, 
                                         'eur', 
                                         `Analisis ${this.tipopedidoseleccionado.nombre}`
        ).then((resultado) => {
          console.log("result de chargePayStripe()");
          console.log(resultado);
          this.pago.set('pagado', true);
          this.pago.set('charged', resultado);
          this.pago.save();
          this.pedido.set('pagado', true);
          let estado_1 = this.estadospedidos.filter(estado => estado.orden == 1 );
          this.pedido.set('pedidoEstado', estado_1);
          this.pedido.save();
          this.router.navigate(['/home/dashboard']);

        }, (err) => {
          console.log("error de chargePayStripe()");
          console.log(err);
          this.pedido.set('pagado', false);
          this.pedido.save();
        });
        

      }, (err) => {
        console.log("error de ng2UploadS3()");
        console.log(err);
      });

     

    }, (error) => {
      console.log("pagarPedido - ERROR");
      console.log(error);

    });
    */

    // solo para test sin entrar en pagarPedido.
    this.ng2UploadS3().then((result) => {
        console.log("result de ng2UploadS3()");
        console.log(result);
        let estado_1 = this.estadospedidos.filter(estado => estado.orden == 3 );
        this.pedido.set('pedidoEstado', estado_1);
        this.pedido.save();
        this.router.navigate(['/home/dashboard']);

    }, (err) => {
        console.log("error de ng2UploadS3()");
        console.log(err);
    });  

    console.log("FIN newpedido()");



  }

/*
  saveModelPedido(){
    // CREAR PEDIDO NUEVO EN BD PARSE
    console.log("saveModelPedido");

    let tipopointer = this.parseobject.createPointerFromId("TipoPedido", this.tipopedidoseleccionado.id);
    console.log(tipopointer);

    let sin_estado = this.estadospedidos.filter(estado => estado.orden == 0 );
    console.log(sin_estado);

    let estadopointer = this.parseobject.createPointerFromId("Estado", sin_estado[0].id);
    console.log(estadopointer);

    // this.videos_uploaded  // array con los pointer a los video en parse.
    // this.pago_saved      // pointer al pago de este pedido en parse.

    this.parseobject.pedidocreate( this.pago_saved, tipopointer, this.pointerusercurrent, estadopointer, this.observaciones).then((result) => {
      console.log("UploadvideoComponent => parseobject.pedidocreate = result");
      console.log(result);

      this.pedido = result;

    // TODO CREAR PAGO CON DATOS DEL TOKEND
    // LLAMAR A PARSE.CLOUD PARA CARGAR EL PAGO DESDE SERVIDOR (LO EXIGE STRIPE)

    }, (error) =>{
      console.log("UploadvideoComponent => parseobject.pedidocreate = error");
      console.log(error);
    });
  }
*/


/*
  saveModelVideo(){
      // creamos LOS VIDEOS en BD PARSE
      console.log("saveModelVideo");
      console.log(this.uploader.queue);
      console.log(this.files_uploaded);

      for (let objectfile of this.files_uploaded ) {
       this.parseobject.videocreate( objectfile.filename, objectfile.urlcloudfront, this.pointerusercurrent).then((result) => {
         console.log("UploadvideoComponent => parseobject.videocreate = result");
         console.log(result);

         let video = result;
         let videopointer = this.parseobject.createPointerFromId("Video", video["id"]);
         console.log(videopointer);
         this.videos_uploaded.push(videopointer);

         this.pedido.set('pedidoVideos', this.videos_uploaded);
         this.pedido.set('pedidoThumbnail', this.videos_uploaded[0].get('url') + this.videos_uploaded[0].get('filename') + this.ext_thumbnail );
         this.pedido.save();

       }, (error) =>{
         console.log("UploadvideoComponent => parseobject.videocreate = error");
         console.log(error);
       });
      }


  }
*/

  saveModelAnalysis(){
      // creamos EL ANALYSIS en BD PARSE
      console.log("saveModelAnalysis");
      console.log(this.uploader.queue);
      console.log(this.files_uploaded);

      for (let objectfile of this.files_uploaded ) {
       this.parseobject.analisiscreate( objectfile.filename, objectfile.urlcloudfront, this.pointerusercurrent, this.observaciones).then((result) => {
         console.log("UploadvideoComponent => parseobject.videocreate = result");
         console.log(result);

         let video = result;
         let videopointer = this.parseobject.createPointerFromId("Analisis", video["id"]);
         console.log(videopointer);
         this.videos_uploaded.push(videopointer);

         this.pedido.set('pedidoAnalisis', this.videos_uploaded[0]);
         //this.pedido.set('pedidoThumbnail', this.videos_uploaded[0].get('url') + this.videos_uploaded[0].get('filename') + this.ext_thumbnail );
         this.pedido.save();

       }, (error) =>{
         console.log("UploadvideoComponent => parseobject.videocreate = error");
         console.log(error);
       });
      }


  }


/*
  saveModelPago() {
    // creamos el Pago en BD PARSE
    console.log("saveModelPago");
    console.log(this.tokencheckout);
    this.parseobject.pagocreate( this.tokencheckout, this.pointerusercurrent, this.importe, 'eur', `Analisis ${this.tipopedidoseleccionado.nombre}`).then((result) => {
      console.log("UploadvideoComponent => parseobject.pagocreate = result");
      console.log(result);

      this.pago = result;
      let pagopointer = this.parseobject.createPointerFromId("Pago", this.pago["id"]);
      console.log(pagopointer);
      this.pago_saved = pagopointer;

      this.pago.set('pagado', false);
      this.pago.save();

      this.saveModelPedido(); // CREAMOS PEDIDO.

    }, (error) =>{
      console.log("UploadvideoComponent => parseobject.pagocreate = error");
      console.log(error);
    });

  }
*/

// Pagar
  // PAGO CON STRIPE
/*  
  pagarPedido(): Promise<any> {
    // LANZAMOS MODAL STRIPE Y REALIZAMOS EL PAGO EN STRIPE

    // class="Header-navClose" span .. es el icono de close form stripe.js

    console.log(`pagarPedido()`);
    return new Promise((resolve, reject) => {
      this.tokencheckout = null;
      let submittedForm = false;
      var handler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_JypfgA197ZRyzvNwf7PPbmeY',
        locale: 'auto',
        token: function (token: any) {
          // You can access the token ID with `token.id`.
          // Get the token ID to your server-side code for use.
          console.log('Token Stripe');
          console.log(token);
          console.log(token.id);
          console.log(token.card);
          //this.tokencheckout = token;
          submittedForm = true;
            //$('#submit-text').html('Processing, stand by');
         //etc functionality to submit back to your own form
           
          resolve(token);

        }
      });

      handler.open({
        name: 'Xot Padel',
        description: `Analisis ${this.tipopedidoseleccionado.nombre}`,
        zipCode: true,
        currency: 'eur',
        amount: this.importe,  // TODO EL PRECIO NO DEBE DE SER HARDCODE
        closed: function () {
              console.log("pagarPedido() --> CLICK CLOSED");
              //if(submittedForm == false){ 
                  // $('#submit-text').html('Start your Trial');
              //}
        }
      });

    });

  }
*/

// ng2-file-upload
  ng2UploadS3(): Promise<any> {
      // LLAMAMOS AL GENERADOR DE URLS Y LANZAMOS EL UPLOAD
      console.log("ng2UploadS3");
      return new Promise((resolve, reject) => {
        console.log(this.uploader);
        this.uploader.options.disableMultipart = true;
        console.log(this.uploader);
        //console.log(this.uploader.queue);
        for (let i in this.uploader.queue) {
          this.uploader.queue[i].file.name = this.uploader[i].queue.file.name.replace(/\s/g, "_")
        };
        
        this.getUrlS3(this.uploader.queue).then((result)=>{
          console.log("getUrlS3 promise  --> RESULT");
          console.log(result);
          console.log(this.uploader.queue);
          this.uploadS3Files().then((result) => { // UPLOAD !!!
              console.log('uploadS3Files --> RESULT ');
              // SE ACABO DE SUBIR TODOS LOS VIDEOS
              resolve(result);
              //this.saveModelVideo(); // save model Video en Parse
              this.saveModelAnalysis(); // save model Analysis en Parse

          }, (err) => {
              console.log('uploadS3Files --> ERROR ');
              console.log(err);
              reject(err);
          }); 

        },(err)=>{
          console.log("getUrlS3 promise --> ERROR");
          console.log(err);
          reject(err);
        });
      });  
  }

  getUrlS3(queue: any):Promise<any> {
        // GENERAR URL PRESIGNED S3 PARA TODO EL QUEUE
        console.log("getUrlS3 --> ");
        console.log(queue);
        let promises_array:Array<any> = [];
        let count = 0;
        for (let file of queue) {
          let uuid_file = UUID.UUID();
          console.log(uuid_file);
          let _name = uuid_file + '_' + file.file.name;
          let splitname = _name.split('.', 2);
          _name = splitname[0];

          // CREAMOS NAMES CON UUID Y URL CON CLOUDFRONT
          this.files_uploaded.push({filename: _name, urlcloudfront: this.cloudfront + _name + '/' });

          let item = {id: count, filename: uuid_file + '_' + file.file.name , filetype: file.file.type, key: this.prefix_S3 };
          promises_array.push(new Promise( (resolve,reject) => {
            // pedimos una url prefirmada de s3 para video_source
            this.parseobject.getsigned_s3(item).then((result) => {
              console.log("parseobject.getsigned_s3 --> RESULT")
              console.log(result);
              //let json_result = JSON.parse(result);
              let json_result = result;
              console.log(json_result.signedRequest);
              this.uploader.queue[item.id].url = json_result.signedRequest;
              console.log(this.uploader.queue[item.id].url);
              resolve(true);
            }, (err) => {
              console.log("parseobject.getsigned_s3 --> ERROR")
              console.log(err);
              reject(err);
            });
          }));
          count = ++count;
        }
      return Promise.all(promises_array);
  }

  uploadS3Files(): Promise<any> {
    // LANZAMOS EL UPLOAD (SUBIDA REAL DE LOS FICHEROS AL SERVIDOR S3)
    console.log(`uploadS3Files()`);
    return new Promise((resolve, reject) => {
      console.log("Iniciamos evento Upload");
      for (let file of this.uploader.queue) {
            file.upload();
      }
      this.uploader.onCompleteAll = () => {
        console.log("onCompleteAll:");
        resolve(true);
      };

    });
  }

}
