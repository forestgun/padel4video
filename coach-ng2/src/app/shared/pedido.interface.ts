export interface Pedido {
   objectId: string;
   fecha: Date;
   userpointer: any;
   videopointer: any;
   analisispointer: any;
   tipopointer: any;
   pagado: boolean;
   pagopointer: any;
   estadopointer: any;
   observaciones: string;
}

/*
export class Pedido {

  public objectId: string;
  public fecha: Date;
  public userpointer: any;
  public videopointer: any;
  public analisispointer: any;
  public tipopointer: any;
  public pagado: boolean;
  public pagopointer: any;
  public estadopointer: any;
  public observaciones: string;

  constructor(
     _objectId: string,
   _fecha: Date,
   _userpointer: any,
   _videopointer: any,
   _analisispointer: any,
   _tipopointer: any,
   _pagado: boolean,
   _pagopointer: any,
   _estadopointer: any,
   _observaciones: string,

  ) {


      this.objectId = _objectId;
      this.fecha = _fecha;
      this.userpointer = _userpointer;
      this.videopointer = _videopointer;
      this.analisispointer = _analisispointer;
      this.tipopointer = _tipopointer;
      this.pagado = _pagado;
      this.pagopointer = _pagopointer;
      this.estadopointer = _estadopointer;
      this.observaciones = _observaciones;


  }


}
*/
