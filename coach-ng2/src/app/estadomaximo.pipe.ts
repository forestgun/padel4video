import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estadomaximo'
})
export class EstadomaximoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    //return null;
    return value.filter(val => val.get('pedidoEstado').get('orden') <= args );
  }

}