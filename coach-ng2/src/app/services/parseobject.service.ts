import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';

import { CurrentUser } from '../shared/CurrentUser';
import { Pedido } from '../shared/pedido.interface';
import { Estadopedido } from '../shared/Estadopedido';
import { Tipopedido } from '../shared/Tipopedido';

import { ParsePointer } from '../shared/ParsePointer';


//const Parse: any = require('parse');
// PARSE
import * as Parse from 'parse';


@Injectable()
export class ParseobjectService  {

  private subject = new ReplaySubject<CurrentUser>();
  public currentuser: CurrentUser;
  public estadospedidos: Array<Estadopedido>;
  public tipospedidos: Array<Tipopedido>;


  //parsepointer: ParsePointer;

  pedidosquery: any;
  subscription: any;
  client: any;
  LiveQueryClient: any;
  parseSession: any;
  pedidosmodelo: Pedido;



  constructor() {
    console.log("ParseobjectService init");
    Parse.initialize('myAppId_9999', '66b0f6086a032');
    Parse.serverURL = 'https://parse4video.herokuapp.com/parse';
    //this.subscription = this.newsSubscription()

    this.estadospedidos = [];  // usar en formularios
    this.tipospedidos = [];    // user en formularios

    this.getalltipopedido().then((result) => {
      console.log("getalltipopedido resuelve con result => ");
      //console.log(result);
      let jsonfile = JSON.stringify(result);
      let object = JSON.parse(jsonfile);
      //console.log(object);
      for (let tipo of object) {
        //console.log("tipo of object");
        //console.log(tipo);
        //console.log(this.tipospedidos);
        this.tipospedidos.push(new Tipopedido(tipo.objectId, tipo.nombre, tipo.activado));
      }
      //console.log(this.tipospedidos);
    }, (error) => {
      console.log("getalltipopedido resuelve con error => ");
      console.error(error);
    });

    this.getallestado().then((result) => {
      console.log("getallestado resuelve con result => ");
      //console.log(result);
      let jsonfile = JSON.stringify(result);
      let object = JSON.parse(jsonfile);
      //console.log(object);
      for (let estado of object) {
        //console.log("estado of object");
        //console.log(estado);
        //console.log(this.estadospedidos);
        this.estadospedidos.push(new Estadopedido(estado.objectId, estado.nombre, estado.orden));
      }
      console.log(this.estadospedidos);
    }, (error) => {
      console.log("getallestado resuelve con error => ");
      console.error(error);
    });


  }

  // UTILIDAD PARSE PARA GENERER POINTER A OBJECT CON SOLO NOMBRE CLASE E ID
  public createPointerFromId = (className: string, objectId: string): ParsePointer => {
    console.log("ParseobjectService => createPointerFromId()");
    console.log(className);
    console.log(objectId);
    let objClass = Parse.Object.extend(className);
    //let obj = new objClass();
    let obj = objClass.createWithoutData(objectId) ;
    //obj.id = objectId;
    console.log(obj);
    return obj;
  };

  public videocreate(filevideo: string, fileurl: string, currentuser: any ){
    return new Promise((resolve, reject) => {
      let videoParse = Parse.Object.extend("Video");
      let videoparse = new videoParse();

      videoparse.set("videoUser", currentuser);
      videoparse.set("filename", filevideo);
      videoparse.set("url", fileurl);

      videoparse.save(null, {
        success: function(videoparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(videoparse);
          //alert('New object created with objectId: ' + videoparse.id);
          resolve(videoparse);
        },
        error: function(videoparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(videoparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });
  }

  // CREATE -- PEDIDO
  public pedidocreate( pagopointer: any, tipopointer: any, currentuser: any, estadopointer: any, observaciones: string ){
    return new Promise((resolve, reject) => {
      let pedidoParse = Parse.Object.extend("Pedido");
      let pedidoparse = new pedidoParse();

      pedidoparse.set("pedidoUser", currentuser);
      //pedidoparse.set("pedidoVideo", videopointer);
      pedidoparse.set("pedidoTipo", tipopointer);
      pedidoparse.set("pedidoEstado", estadopointer);
      pedidoparse.set("pedidoPago", pagopointer);
      pedidoparse.set("Observaciones", observaciones);

      pedidoparse.save(null, {
        success: function(pedidoparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(pedidoparse);
          //alert('New object created with objectId: ' + videoparse.id);
          resolve(pedidoparse);
        },
        error: function(pedidoparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(pedidoparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });
  }

  // GET -- PEDIDO
  public getPedido(id: any){
    console.log('getPedido(id) --> id == ');
    console.log(id);
    return new Promise((resolve, reject) => {
      let pedidoParse = Parse.Object.extend("Pedido");
      let pedidoparse = new Parse.Query(pedidoParse);
      //tipopedidoparse.equalTo();
      pedidoparse.include("pedidoUser");
      pedidoparse.include("pedidoVideos");
      pedidoparse.include("pedidoAnalisis");
      pedidoparse.include("pedidoTipo");
      pedidoparse.include("pedidoPago");
      pedidoparse.include("pedidoEstado");
      pedidoparse.get(id, {
        success: function(pedidoparse) {
          // Execute any logic that should take place after the object is saved.
          //console.log(tipopedidoparse);
          resolve(pedidoparse);
        },
        error: function(pedidoparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(pedidoparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }


  // GET -- TIPO PEDIDO
  public getalltipopedido(){
    return new Promise((resolve, reject) => {
      let tipopedidoParse = Parse.Object.extend("TipoPedido");
      let tipopedidoparse = new Parse.Query(tipopedidoParse);
      //tipopedidoparse.equalTo();
      tipopedidoparse.find( {
        success: function(tipopedidoparse) {
          // Execute any logic that should take place after the object is saved.
          //console.log(tipopedidoparse);
          resolve(tipopedidoparse);
        },
        error: function(tipopedidoparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(tipopedidoparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }

  public gettipopedido(idtipopedido: string){

  }

  // GET -- ESTADO
  public getallestado(){
    return new Promise((resolve, reject) => {
      let estadoParse = Parse.Object.extend("Estado");
      let estadoparse = new Parse.Query(estadoParse);
      estadoparse.ascending("orden");
      //tipopedidoparse.equalTo();
      estadoparse.find( {
        success: function(estadoparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(estadoparse);
          resolve(estadoparse);
        },
        error: function(estadoparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(estadoparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }

  public getestado(idestado: string){

  }


  // CREATE -- PAGO
  public pagocreate( tokencheckout: any, currentuser: any, amount: any, currency: any, description: any) {
    return new Promise((resolve, reject) => {
      let pagoParse = Parse.Object.extend("Pago");
      let pagoparse = new pagoParse();

      pagoparse.set("pagoUser", currentuser);
      pagoparse.set("tokenCheckout", tokencheckout);
      pagoparse.set("token_id", tokencheckout.id);
      pagoparse.set("importe", amount);
      pagoparse.set("moneda", currency);
      pagoparse.set("concepto", description);

      pagoparse.save(null, {
        success: function(pagoparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(pagoparse);
          //alert('New object created with objectId: ' + videoparse.id);
          resolve(pagoparse);
        },
        error: function(pagoparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(pagoparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });

  }

  // CREATE -- ANALISIS
  public analisiscreate(filevideo: string, fileurl: string, currentuser: any, informe: any ){
    return new Promise((resolve, reject) => {
      let analisisParse = Parse.Object.extend("Analisis");
      let analisisparse = new analisisParse();

      analisisparse.set("analisisUser", currentuser);
      analisisparse.set("videoAnalisis", filevideo);
      analisisparse.set("urlAnalisis", fileurl);
      analisisparse.set("informe", informe);

      analisisparse.save(null, {
        success: function(analisisparse) {
          // Execute any logic that should take place after the object is saved.
          console.log(analisisparse);
          //alert('New object created with objectId: ' + videoparse.id);
          resolve(analisisparse);
        },
        error: function(analisisparse, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log(analisisparse);
          console.log(error);
          //alert('Failed to create new object, with error code: ' + error.message);
          reject(error);
        }
      });
    });
  }

  // CHARGE -- STRIPE PRE PAGO
  public chargePayStripe(token: any, currentuser_email: any, amount: any, currency: any, description: any) {
    //return new Promise((resolve, reject) => {
      /*
        const token = req.params['token'];
        const email_customer = req.params['email'];
        const amount = req.params['amount'];
        const currency = req.params['currency'];
        const description = req.params['description'];
      */
      let params = {
          token:  token,
          email:  currentuser_email,
          amount: amount,
          currency: currency,
          description: description
      };

      return Parse.Cloud.run('charge', params);



    //});

  }

  // GET -- PEDIDOS (INCLUDES: VIDEO, ANALISIS, PAGO, TIPOPEDIDO, ESTADO)
  // este esta incluido en parselivequery.service.ts



/*
  <!-- Pedido.pedidoUser (pointer) -->
  <!-- Pedido.pedidoTipo  (pointer) Tipo.nombre -->
  <!-- Pedido.observaciones -->
  <!-- Pedido.pagado -->
  <!-- Pedido.pedidoPago (pointer) -->
  <!-- Pedido.pedidoEstado (pointer)  Estado.nombre-->
  <!-- Pedido.pedidoVideo (pointer)  -->
  <!-- Pago.importe -->
  <!-- Pago.referencia -->
  <!-- Pago.pagado -->
  <!-- Pago.pagoUser  (pointer)  -->
  <!-- Pago.tarjeta (pointer -->
  <!-- Video.filename -->
  <!-- Video.url -->
  <!-- Video.videoUser (pointer) -->
  <!-- Analisis.videoAnalisis (string nombre fichero video analisis final) -->
  <!-- Analisis.analisisUser  (pointer) -->
  <!-- Analisis.urlAnalisis  (string url localizacion de videoAnalisis) -->
  <!-- Analisis.informe   (string texto del analisis) -->
  <!-- Tarjeta.tarjetaUser  (pointer) -->
*/
    /*
    * Devuelve una url autorizada para subir ficheros a aws s3
    * @params =   params['file-name'] params['file-type'];
    * params= { file-name: "", file-type: ""};
    */
    public getsigned_s3(params: any) {
        console.log("getsigned_s3 --> ");
        console.log(params);
        return Parse.Cloud.run('sign-s3', params);
        /*
            {"result":
                {"signedRequest":"https://padel4video403015pp.s3.amazonaws.com/test_parse_server?AWSAccessKeyId=AKIAIWNNOJO2CDM25SQA&Content-Type=png&Expires=1496733277&Signature=y11ZT%2FnGpJ6So07WD4m7%2F0wEZP8%3D&x-amz-acl=public-read",
                "url":"https://padel4video403015pp.s3.amazonaws.com/test_parse_server"
                }
            }
        */

    }

  // FIND -- TUS PAGOS
  public getTusPagos(userpointer: any){
    return new Promise((resolve, reject) => {
      let pagosParse = Parse.Object.extend("Pago");
      let pagosparse = new Parse.Query(pagosParse);
      pagosparse.descending("createdAt");
      pagosparse.equalTo("pagoUser", userpointer);
      /*
      pagosparse.find({
        success: function(pagos){
          console.log("success query find pagos");
          console.log(pagos);
          resolve(pagos);

        },
        error: function(error){
          console.log("error query find pagos");
          console.log(error);
          reject(error);
        }
      });
      */
      pagosparse.find().then(function(pagos) {
          console.log("success query find pagos");
          console.log(pagos);
          console.log(pagos.length);
          resolve(pagos);
      }, function(err) {
          console.log("error query find pagos");
          console.log(err);
          reject(err);
      });

    });

  }

}
