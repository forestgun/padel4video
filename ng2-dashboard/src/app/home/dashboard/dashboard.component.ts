import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ParselivequeryService } from '../../services/parselivequery.service';
import { Pedido } from '../../shared/pedido.interface';




@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  pedidos_modelo: Array<any> = [];


  constructor( private parselive: ParselivequeryService, 
               //private service: HeroService,
              private route: ActivatedRoute,
              private router: Router
               ) {


  }

  ngOnInit() {
    this.pedidos_modelo = [];
    this.getpedidos();
    this.observarpedidos();


  }

  getpedidos(){ // TODO DESPUES DE AÑADIR EL PRIMER PEDIDO Y ENTRAR POR AQUI ..
                // EN EL HTML DA ERRORES DE .GET() ... Y SOLO MUESTRA THUMBNAIL 
                // REVISAR ..
                // SI REFRESCO ENTONCES FUNCIONA Y NO DA ERROR.
    console.log("DASHBOARD - getpedido()");
    this.parselive.getPedidos().subscribe( pedidos => {
      this.pedidos_modelo = pedidos;
      console.log("DASHBOARD - getpedido().subscribe => this.pedidos_modelo");
      console.log(this.pedidos_modelo);
      console.log(this.pedidos_modelo[0]);
      console.log(this.pedidos_modelo[0]["id"]);
      console.log(this.pedidos_modelo[0].get('pedidoTipo').get('nombre'));
      console.log(this.pedidos_modelo[0].get('Observaciones'));
    });
  }

  observarpedidos(){
    console.log("DASHBOARD - observarpedidos()");
    this.parselive.startUpdate()
      .subscribe( pedido =>{
        //this.zone.run(() => {
        console.log(`DASHBOARD pedidos => ${JSON.stringify(pedido)}`);
        //})
      });
  }

  onSelect(pedido: any) {
    this.router.navigate(['/home/pedido', pedido['id'] ]); // 
  }

}
