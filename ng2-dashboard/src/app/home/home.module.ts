
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {MomentModule, DateFormatPipe} from 'angular2-moment';
import 'moment/locale/es';

import { MaterialModule } from '@angular/material';
import { MdProgressSpinnerModule, MdProgressBarModule } from '@angular/material';
import 'hammerjs';
import { FlexLayoutModule } from '@angular/flex-layout';

import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import { VgStreamingModule } from 'videogular2/streaming';

//import { NgUploaderModule } from 'ngx-uploader';
//import { FileUploader, FileSelectDirective} from 'ng2-file-upload';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';


//import { UUID } from 'angular2-uuid';

import { ParsemanagerService } from '../services/parsemanager.service';
import { ParselivequeryService } from '../services/parselivequery.service';
import { ParseobjectService } from '../services/parseobject.service';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponentComponent } from './home-component/home-component.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ProfileComponent } from './profile/profile.component';
import { UploadvideoComponent } from './uploadvideo/uploadvideo.component';
import { PedidoComponent } from './pedido/pedido.component';

import { InputFile } from './uploadvideo/inputFile';
import { EstadomaximoPipe } from './../estadomaximo.pipe';
import { AnalizadosPipe } from './../analizados.pipe';

//import { CurrentUser } from '../shared/CurrentUser';
//import { Pedido } from '../shared/Pedido';
//import { Estadopedido } from '../shared/Estadopedido';
//import { Tipopedido } from '../shared/Tipopedido';

//import { ParsePointer } from '../shared/ParsePointer';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MomentModule,
    MaterialModule,
    MdProgressSpinnerModule,
    FlexLayoutModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    VgStreamingModule,
    //NgUploaderModule,
    FileUploadModule,
 
    //UUID,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponentComponent,
    DashboardComponent,
    SidebarComponent,
    ProfileComponent,
    UploadvideoComponent,
    InputFile,
    PedidoComponent,
    AnalizadosPipe,
    EstadomaximoPipe
//    CurrentUser,
//    Pedido,
//    Estadopedido,
//    Tipopedido,
//    ParsePointer
  ],
  providers: [ParsemanagerService, ParselivequeryService, ParseobjectService]
})
export class HomeModule { }
