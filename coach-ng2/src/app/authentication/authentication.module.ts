import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '@angular/material';
import 'hammerjs';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ParsemanagerService } from '../services/parsemanager.service';


import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationComponentComponent } from './authentication-component/authentication-component.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { SignupComponentComponent } from './signup-component/signup-component.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    AuthenticationRoutingModule
  ],
  declarations: [AuthenticationComponentComponent, LoginComponentComponent, SignupComponentComponent]
})
export class AuthenticationModule { }
