import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ParsemanagerService } from '../../services/parsemanager.service';
// import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-signup-component',
  templateUrl: './signup-component.component.html',
  styleUrls: ['./signup-component.component.css']
})
export class SignupComponentComponent implements OnInit {
  private user: any;
  username: string = '';
  password: string = '';
  password2: string;

  loading = false ;
  nombre: string;
  apellidos: string;
  nif: string;
  direccion: string;



  constructor(private rutas: Router, private parsemanager: ParsemanagerService) { }

  ngOnInit() {
        console.log("SignupComponentComponent init");
        //let user = this.parsemanager.init();
        //console.log(`user => ${JSON.stringify(user)}`);
       if (this.parsemanager.isAuthenticated()) {
          console.log ("isAuthenticated => true");
          this.rutas.navigate(['home']);
       } else {
          console.log ("isAuthenticated => false");
          //this.rutas.navigate(['auth']);
       };
  }


  signup(username: string, password: string) {
      this.parsemanager.signup(username, password).then(
        user => {
          console.log("SignUP --> signup()=> RESULT");
          console.log(user);
          user[0].set('nombre', this.nombre);
          user[0].set('apellidos', this.apellidos);
          user[0].set('nif', this.nif);
          user[0].set('direccion', this.direccion);
          user[0].set('email', this.username);

          user[0].save();

          this.loading = false;
          this.isAuth();
        },
        err => {
          this.loading = false;
          this.isAuth();
        }
      );
      //this.isAuth();
  }

  isAuth(){
       if (this.parsemanager.isAuthenticated()) {
          console.log ("isAuthenticated => true");
          this.rutas.navigate(['home']);
       } else {
          console.log ("isAuthenticated => false");
          this.rutas.navigate(['auth']);
       };
  }

}
