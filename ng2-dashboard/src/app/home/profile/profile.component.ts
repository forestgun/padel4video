import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ParseobjectService } from '../../services/parseobject.service';

import * as Parse from 'parse';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  usercurrent: any;
  pointerusercurrent: any;
  pagos: Array<any>;

  constructor(private parseobject: ParseobjectService,
              private router: Router
  ) {
    this.usercurrent = Parse.User.current();
    this.pointerusercurrent = this.usercurrent.toPointer();
    this.pagos = [];

  }

  ngOnInit() {

    // public getTusPagos(userpointer: any)
    this.parseobject.getTusPagos(this.pointerusercurrent).then((result) => {
      console.log(" parseobject.getTusPagos() --> RESULT ");
      console.log(result);
      this.pagos = this.pagos.concat(result); // metodo de pasar result a un array
      

    }, (err) => {
      console.log(" parseobject.getTusPagos() --> ERROR ");
      console.log(err);
    });

  }

}
