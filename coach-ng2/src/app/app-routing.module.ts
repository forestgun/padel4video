import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
// import { AuthenticationModule } from './authentication/authentication.module';
// import { HomeModule } from './home/home.module';

const routes: Routes = [
  {path: '', component: AppComponent }
  /* ,
  {path: 'home', component: HomeModule },
  {path: 'auth', component: AuthenticationModule }
  */ // las routas que estan definidas en modulos no se definen aqui
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
