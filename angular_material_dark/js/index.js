angular
  .module('MyApp',['ngSanitize', 'ngMaterial', 'ngMessages', 'material.svgAssetsCache', 'ngVidBg', 'duParallax', 'duScroll', 'ngRoute'])
//.module('MyApp',['ngSanitize', 'ngMaterial', 'ngMessages', 'material.svgAssetsCache', 'ngVidBg', 'ngParallax', 'ngRoute'])
    .config(function($mdThemingProvider, $mdIconProvider, $routeProvider, $locationProvider) {

    //disable theme generation
    $mdThemingProvider.generateThemesOnDemand(false);


    $mdThemingProvider.theme('altTheme')
          .primaryPalette('yellow',{
              'default': '500'})
          .accentPalette('yellow',{
              'default': 'A200'})
          .warnPalette('orange',{
              'default': '900'})
          .backgroundPalette('grey')
          .dark();

        /*
         .primaryPalette('amber',{
         'default': '500'})
         .accentPalette('amber',{
         'default': 'A200'})
         .warnPalette('orange',{
         'default': '900'})
         .backgroundPalette('grey')
         .dark();
         */

      /*
      .primaryPalette('grey',{
       'default': '900'})
      .accentPalette('grey',{
       'default': '600'})
      .warnPalette('orange',{
       'default': '500'})
      .backgroundPalette('grey',{
       'default': '300'})
    .dark();
     */


  
  // .backgroundPalette('brown',{'default': '50'})
  

//  <!-- orson 說太紅了 要灰的  --> 
//   $mdThemingProvider.theme('altTheme')
//        .primaryPalette('red',{
//       'default': '400'})
//        .accentPalette('deep-purple',{
//       'default': 'A700'})
//        .backgroundPalette('brown',{
//       'default': '50'})
// ;

  //$mdThemingProvider.theme('default');
  // .dark();

        $mdThemingProvider.theme('default')
            .primaryPalette('blue',{
                'default': '500'})
            .accentPalette('blue',{
                'default': 'A200'})
            .warnPalette('red',{
                'default': '900'})
            .backgroundPalette('grey')
            .dark();

  //   // .primaryPalette('pink')
  //   .backgroundPalette('red',{
  //     'default': '200'})
  //   .accentPalette('blue');

    $mdThemingProvider.setDefaultTheme('altTheme');
    $mdThemingProvider.alwaysWatchTheme(true);


    //$mdIconProvider
    //    .iconSet('social', 'img/icons/sets/social-icons.svg', 24)
    //    .defaultIconSet('img/icons/sets/core-icons.svg', 24);
        $mdIconProvider
            .iconSet('action', 'img/icons/iconsets/action-icons.svg', 24)
            .iconSet('alert', 'img/icons/iconsets/alert-icons.svg', 24)
            .iconSet('av', 'img/icons/iconsets/av-icons.svg', 24)
            .iconSet('communication', 'img/icons/iconsets/communication-icons.svg', 24)
            .iconSet('content', 'img/icons/iconsets/content-icons.svg', 24)
            .iconSet('device', 'img/icons/iconsets/device-icons.svg', 24)
            .iconSet('editor', 'img/icons/iconsets/editor-icons.svg', 24)
            .iconSet('file', 'img/icons/iconsets/file-icons.svg', 24)
            .iconSet('hardware', 'img/icons/iconsets/hardware-icons.svg', 24)
            .iconSet('icons', 'img/icons/iconsets/icons-icons.svg', 24)
            .iconSet('image', 'img/icons/iconsets/image-icons.svg', 24)
            .iconSet('maps', 'img/icons/iconsets/maps-icons.svg', 24)
            .iconSet('navigation', 'img/icons/iconsets/navigation-icons.svg', 24)
            .iconSet('notification', 'img/icons/iconsets/notification-icons.svg', 24)
            .iconSet('social', 'img/icons/iconsets/social-icons.svg', 24)
            .iconSet('toggle', 'img/icons/iconsets/toggle-icons.svg', 24);
/*
        $routeProvider
            .when('/dark/:bookId', {
                templateUrl: 'book.html',
                controller: 'BookController',
                resolve: {
                    // I will cause a 1 second delay
                    delay: function($q, $timeout) {
                        var delay = $q.defer();
                        $timeout(delay.resolve, 1000);
                        return delay.promise;
                    }
                }
            })
            .when('/dark/:bookId/ch/:chapterId', {
                templateUrl: 'chapter.html',
                controller: 'ChapterController'
            });

        // configure html5 to get links working on jsfiddle
        $locationProvider.html5Mode(true);
*/

        $locationProvider.hashPrefix('');

})


  //.controller('AppCtrl', function ($scope, $timeout, $mdSidenav, $mdColorPalette, $mdColors, $log, parallaxHelper) {
  .controller('AppCtrl', function ($scope, $timeout, $mdSidenav, $mdColorPalette, $mdColors, $log, parallaxHelper ) {
    /*
    $scope.toggleLeft = buildDelayedToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function(){
      return $mdSidenav('right').isOpen();
    };
    $scope.dynamicTheme = 'altTheme';
    */


      // ---- VIDEO --- ///
      $scope.resources = [
         // 'http://techslides.com/demos/sample-videos/small.webm',
         // 'http://techslides.com/demos/sample-videos/small.ogv',
          'videoplayback.mp4'
      ],
          $scope.poster = 'Entrenar-el-Padel.jpg',
          $scope.fullScreen = false,
          $scope.muted = true,
          $scope.zIndex = '500',
          $scope.pausePlay = false,
          $scope.control = false,
          $scope.currentResourceIdx = 0;
      $scope.playInfo = {};
      // ---- FIN - VIDEO --- //



      $scope.background = parallaxHelper.createAnimator(-0.2);


    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    /*
    function debounce(func, wait, context) {
      var timer;

      return function debounced() {
        var context = $scope,
            args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function() {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    }
    */

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    /*
    function buildDelayedToggler(navID) {
      return debounce(function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }, 200);
    }
    function buildToggler(navID) {
      return function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }
    }
    */

  });

  /*
  .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });

    };
  })
  */

  /*
  .controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      $mdSidenav('right').close()
        .then(function () {
          $log.debug("close RIGHT is done");
        });
    };
  });
  */

/**
Copyright 2016 Google Inc. All Rights Reserved. 
Use of this source code is governed by an MIT-style license that can be in foundin the LICENSE file at http://material.angularjs.org/license.
**/