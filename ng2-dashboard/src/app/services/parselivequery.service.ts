import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { CurrentUser } from '../shared/CurrentUser';

import { Pedido } from '../shared/pedido.interface';
import { ParsemanagerService } from './parsemanager.service';
import { ParseobjectService  } from './parseobject.service';



//const Parse: any = require('parse');
// PARSE
import * as Parse from 'parse';


@Injectable()
export class ParselivequeryService  {
  private subject = new ReplaySubject<CurrentUser>();

  pedidosquery: any;
  subscription: any;
  client: any;
  LiveQueryClient: any;
  parseSession: any;
  public modelopedidos: Array<any>  ;
  parseUserCurrent: any;
  //pedido: Pedido;

  constructor(private parsemanager: ParsemanagerService, private parseobject: ParseobjectService) {
    console.log("ParselivequeryService init");
    //Parse.initialize('myAppId_9999', '66b0f6086a032');
    //Parse.serverURL = 'http://parse4video.herokuapp.com/parse';
    //this.subscription = this.newsSubscription()

    this.modelopedidos = [];
    this.parseSession = "";
    console.log(this.parsemanager.currentUser);
    let currentuser = this.parsemanager.currentUser;
    console.log(`currentuser ${JSON.stringify(currentuser)}`);
    if (currentuser) {
      console.log(`  ParselivequeryService currentuser => ${currentuser}  ` );
      console.log(currentuser);
      console.log(currentuser.id);
      //let currentuserId = this.parsemanager.currentUser.id;
      let currentuserId = currentuser.id;
      console.log(`  ParselivequeryService currentuserId => ${currentuserId}  ` );
      //this.parseUserCurrent = this.parseobject.createPointerFromId('User', currentuserId) ;
      //console.log(" this.parseUserCurrent => ");
      //console.log(this.parseUserCurrent);
      if(currentuserId) {
        console.log(`currentuserId ${currentuserId}`);
        this.parseUserCurrent = currentuser.toPointer();
        console.log(this.parseUserCurrent);
        this.parseSession = currentuser.sessionToken;
      }
    }


    //this.parseSession = parsemanager.currentUser.sessionToken;

    console.log("  this.LiveQueryClient = Parse.LiveQueryClient ");
    this.LiveQueryClient = Parse.LiveQueryClient;
    this.client = new this.LiveQueryClient({
      applicationId: 'myAppId_9999',
      serverURL: 'wss://parse4video.herokuapp.com/parse',
      javascriptKey: '66b0f6086a032',
      masterKey:  '' //'myMasterKey_6666'
    });
    this.startControl()
      .subscribe( (result) =>{
        console.log(`startControl => ${JSON.stringify(result)}`)
      });
    console.log("client.open() launch");
    this.client.open();
    this.pedidosquery = new Parse.Query('Pedido');
    this.pedidosquery.include("pedidoUser");
    this.pedidosquery.include("pedidoVideos");
    this.pedidosquery.include("pedidoAnalisis");
    this.pedidosquery.include("pedidoTipo");
    this.pedidosquery.include("pedidoPago");
    this.pedidosquery.include("pedidoEstado");
    this.pedidosquery.descending("createdAt");
    this.pedidosquery.equalTo('pedidoUser', this.parseUserCurrent); // TODO PENDIENTE DE VERIFICAR SI FILTA POR CURRENT USER
    console.log(`Creamos pedidosquery => ${JSON.stringify(this.pedidosquery)}` );
    this.getPedidos().subscribe( pedidos => {
      this.modelopedidos = pedidos;
      console.log("ParselivequeryService - getpedido().subscribe => this.modelopedidos");
      console.log(this.modelopedidos);
    });
    let newsubscription = this.newsSubscription();
    console.log(`newsubscription => ${JSON.stringify(newsubscription)}`);





  }

  public newsSubscription() {
    console.log(`newsSubscription 1 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
    if (!this.pedidosquery) {
      //this.pedidosquery = new Parse.Query('Pedido');
      //console.log(`newsSubscription 2 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
      //this.loadPedidos();
    }
    // this.pedidosQuery.equalTo('title', 'broadcast');
    console.log(`newsSubscription 3 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
    this.subscription = this.client.subscribe(this.pedidosquery,this.parseSession);
    return this.subscription
  }

  public startUpdate(): Observable<any> {
    console.log(`call startUpdate() ` );
    return new Observable( observer => {

      // CREATE
      this.subscription.on('create', (pedidos) => {
        console.log(`call this.subscription.on(create) ` );
        let pedido: Pedido =  {
            objectId: pedidos['id'],
            fecha: pedidos.get('createdAt'),
            userpointer: pedidos.get('pedidoUser'),
            videopointer: pedidos.get('pedidoVideo'),
            analisispointer: pedidos.get('pedidoAnalisis'),
            tipopointer: pedidos.get('pedidoTipo'),
            pagado: pedidos.get('pagado'),
            pagopointer: pedidos.get('pedidoPago'),
            estadopointer: pedidos.get('pedidoEstado'),
          observaciones: pedidos.get('Observaciones')};

        /*
        pedido.objectId = pedidos.get('objectId');
        pedido.fecha = pedidos.get('createdAt');
        pedido.userpointer = pedidos.get('pedidoUser');
        pedido.videopointer = pedidos.get('pedidoVideo');
        pedido.analisispointer = pedidos.get('pedidoAnalisis');
        pedido.tipopointer = pedidos.get('pedidoTipo');
        pedido.pagado = pedidos.get('pagado');
        pedido.pagopointer = pedidos.get('pedidoPago');
        pedido.estadopointer = pedidos.get('pedidoEstado');
        pedido.observaciones = pedidos.get('Observaciones');
        */
        console.log(pedido);
        //this.modelopedidos.push(pedido);

        console.log(`PEDIDO create => ${JSON.stringify(pedido)}` );
        observer.next(pedido);
      });

      // UPDATE
      this.subscription.on('update', (pedidos) => {
        console.log(`call this.subscription.on(update) ` );
        console.log(pedidos);
        //let test = this.modelopedidos.filter(pedido => pedido.objectId == pedidos.get("objectId"));
        //console.log(test);

        observer.next(pedidos);
      });

      // CLOSE
      this.subscription.on('close', () => {
        console.log(`call this.subscription.on(close) ` );
        observer.next();
      });

      // DELETE
      this.subscription.on('delete', (pedidos) => {
        console.log(`call this.subscription.on(delete) ` );
        observer.next(pedidos);
      });

      // LEAVE
      this.subscription.on('leave', (pedidos) => {
        console.log(`call this.subscription.on(leave) ` );
        observer.next(pedidos);
      });

      // ENTER
      this.subscription.on('enter', (pedidos) => {
        console.log(`call this.subscription.on(enter) ` );
        observer.next(pedidos);
      });

      // OPEN
      this.subscription.on('open', () => {
        console.log(`call this.subscription.on(open) ` );
        observer.next();
      });

    })
  }

  public startControl(): Observable<string> {
    console.log(`call startControl() ` );
    return new Observable( observer => {
      this.client.on('open', () => {
        console.log(`call this.client.on(open) ` );
        observer.next('open');
      });
      this.client.on('close', () => {
        console.log(`call this.client.on(close) ` );
        observer.next('close');
      });
      this.client.on('error', () => {
        console.log(`call this.client.on(error) ` );
        observer.next('error');
      });
      // TODO: other events
      // this.subscription.on('update', (news) => {
      //   this.zone.run(()=> {
      //     this.title = news.get('message')
      //   })
      // })
    })
  }

  public stopUpdate() {
    this.subscription.unsubscribe();
    this.client.close();
  }

  public getModelPedidos(){
    console.log("getModelPedidos() ");
    return this._getPedidos();
  }

  private _getPedidos(): Observable<any>{
    return new Observable( observer => {
      console.log('_getPedidos');
      console.log(this.modelopedidos);
      observer.next(this.modelopedidos)
    })

  }

  public getPedidos(){
    console.log("getPedidos() ");

    return this.loadPedidos();


    /*
    this.loadPedidos().subscribe(success => {
      console.log("getPedidos().subscribe.success => ");
      console.log(success);
    }, error => {
      console.log("getPedidos().subscribe.error => ");
      console.log(error);
    });
    */
    /*
    if(this.modelopedidos != null)
    {
      console.log("getPedidos => this.modelopedidos != null");
      //return Observable.of(this.modelopedidos);
    }
    else
    {
      console.log("getPedidos => this.modelopedidos = null");
      this.loadPedidos().subscribe(success => {
        console.log("getPedidos().subscribe.success => ");
        console.log(success);
      }, error => {
        console.log("getPedidos().subscribe.error => ");
        console.log(error);
      });
      //return Observable.of(this.modelopedidos);
    }
    */

  }

  public loadPedidos(): Observable<any> {
    return new Observable( observer => {
      this.pedidosquery.find().then(function (results) {
        console.log('loadPedidos() : results => ' + JSON.stringify(results));
        /*
        console.log(results.length);
        let arraypedidos = [];
        for (let item of results){
          let pedido: Pedido = {
            objectId: item['id'],
            fecha: item.get('createdAt'),
            userpointer: item.get('pedidoUser'),
            videopointer: item.get('pedidoVideo'),
            analisispointer: item.get('pedidoAnalisis'),
            tipopointer: item.get('pedidoTipo'),
            pagado: item.get('pagado'),
            pagopointer: item.get('pedidoPago'),
            estadopointer: item.get('pedidoEstado'),
            observaciones: item.get('Observaciones')
          };
          //console.log(pedido);
          arraypedidos.push(pedido) ;

        }
        //console.log("arraypedidos");
        //console.log(arraypedidos);
        observer.next(arraypedidos);
        */
        observer.next(results);
      }, function (error) {
        console.log('loadPedidos() : error => ' + JSON.stringify(error));
        observer.error(error)

      });
    });

  }



/*
    console.log("ParselivequeryService init");
    this.parseSession = Parse.Session.current();
    console.log(`parseSession => ${JSON.stringify(this.parseSession)}`);
    this.LiveQueryClient = Parse.LiveQueryClient;

    this.client = new this.LiveQueryClient({
      applicationId: 'myAppId_6666',
      serverURL: 'ws://parse4padel.herokuapp.com/parse',
      javascriptKey: '99b0f6086a032',
      masterKey:  '' //'myMasterKey_6666'
    });

    this.client.on('open', () => {
      console.log('connection opened');
    });
    this.client.on('close', () => {
      console.log('connection closed');
    });
    this.client.on('error', (error) => {
      console.log('connection error');
    });

    this.client.open(); // conectamos con LIVEQUERY mediante WebSocket


    this.query_1 = new Parse.Query('Pedido');

    this.subscription = this.client.subscribe(this.query_1, this.parseSession..get('sessionToken'));



    this.query_1.find().then(function (results) {
      console.log('ParseLIVEFACTORY_QUERY.find(results) => ' + JSON.stringify(results) );
      //angular.forEach (results, function(value, key){
        //console.log(JSON.stringify(key) + ': ' + JSON.stringify(value));

        //var item = {};
        //item['fechaItem'] = value.get('Fecha');
        //item['objectIdItem'] = value.id;
        //item['_partido'] = value;

        //collection.push(item);

        //console.log('PartidoTipo: => ' + JSON.stringify(value.get('tipo')));
        //console.log('Club: => ' + JSON.stringify(value.get('club')));
      //});
      //console.log('PARTIDOSLIVEFACTORY_QUERY.find(results) => collection ' + JSON.stringify(collection) );
      //defer.resolve(collection);
    }, function (error) {
      console.log('ParseLiveFactory: error => ' + JSON.stringify(error));
      //defer.reject(error);
    });

    this.subscription.on('open', () => {
      console.log('subscription opened');
    });
    this.subscription.on('create', (object) => {
      console.log('object created');
    });
    this.subscription.on('update', (object) => {
      console.log('object updated');
    });
    this.subscription.on('enter', (object) => {
      console.log('object entered');
    });
    this.subscription.on('leave', (object) => {
      console.log('object left');
    });
    this.subscription.on('delete', (object) => {
      console.log('object deleted');
    });
    this.subscription.on('close', () => {
      console.log('subscription closed');
    });


  }
*/
  public getmodelopedido(id: string){
    console.log("getmodelopedido");
    //console.log(this.modelopedidos);

    let pedido = this.modelopedidos.filter(val => val['id'] === id);
    //console.log(pedido);
    if (pedido.length > 0){
      return pedido
    } else {
      return []
    }

  }

}
