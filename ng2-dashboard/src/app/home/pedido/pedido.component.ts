import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { ParselivequeryService } from '../../services/parselivequery.service';
import { Pedido } from '../../shared/pedido.interface';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

  pedido: Array<any> = [];
  videos: Array<any> = [];
  vgdash1: string;
  vghls1: string;
  vgmp41: string;
  vgdash2: string;
  vghls2: string;
  vgmp42: string;
  vgdash_analisis: string;
  vghls_analisis: string;
  vgmp4_analisis: string;
  item: {};
  analisis: any;

  constructor(  private parselive: ParselivequeryService,
                private route: ActivatedRoute,
                private router: Router
                //,
                //private service: HeroService
            )
  { 
    this.vgdash1 = '';
    this.vghls1 = '';
    this.vgmp41 = '';
    this.vgdash2 = '';
    this.vghls2 = '';
    this.vgmp42 = '';

    this.vgdash_analisis = '';
    this.vghls_analisis = '';
    this.vgmp4_analisis = '';

  }

  ngOnInit() {
    console.log("PedidoComponent Inicio");
    /*
    this.route.params
    // (+) converts string 'id' to a number
    .switchMap((params: Params) => this.service.getHero(+params['id']))
    .subscribe((hero: Hero) => this.hero = hero);
    */
      // (+) converts string 'id' to a number
    let id = this.route.snapshot.params['id'];
    //console.log(id);
    //console.log(this.route.snapshot.params);
    this.pedido = this.parselive.getmodelopedido(id);
    console.log(this.pedido);
    if(this.pedido.length > 0 ) {
      this.item = this.pedido[0];
      console.log("item pedidos -->");
      console.log(this.item);

      let filename1 = this.item['attributes']['pedidoVideos'][0]['attributes']['filename'];
      let urldash1 = this.item['attributes']['pedidoVideos'][0]['attributes']['url'];
      this.vgdash1 =  urldash1 + filename1 + '-master-playlist-dash.mpd';
      this.vghls1 =  urldash1 + filename1 + '-master-playlist-hls.m3u8';
      this.vgmp41 =  urldash1 + filename1 + '.mp4';
      if (this.item['attributes']['pedidoVideos'].length >= 2) {
        let filename2 = this.item['attributes']['pedidoVideos'][1]['attributes']['filename'];
        let urldash2 = this.item['attributes']['pedidoVideos'][1]['attributes']['url'];
        this.vgdash2 =  urldash2 + filename2 + '-master-playlist-dash.mpd';
        this.vghls2 =  urldash2 + filename2 + '-master-playlist-hls.m3u8';
        this.vgmp42 =  urldash2 + filename2 + '.mp4';
      } else { 
        // solo tiene subido 1 video

      }

      if (this.item['attributes']['pedidoAnalisis']) {
        // El pedido ya tiene una Analisis
        this.analisis = this.item['attributes']['pedidoAnalisis'];

        let filenameAnalisis = this.item['attributes']['pedidoAnalisis']['attributes']['videoAnalisis'];
        let urldashAnalisis = this.item['attributes']['pedidoAnalisis']['attributes']['urlAnalisis'];
        this.vgdash_analisis =  urldashAnalisis + filenameAnalisis + '-master-playlist-dash.mpd';
        this.vghls_analisis =  urldashAnalisis + filenameAnalisis + '-master-playlist-hls.m3u8';
        this.vgmp4_analisis =  urldashAnalisis + filenameAnalisis + '.mp4';

      } else {
        // El pedido no tiene analisis enlazado.


      }

    } else {
        // pedido no existe

    }


    

  }

  gotoDashboard() {
    this.router.navigate(['dashboard']);
  }


}
