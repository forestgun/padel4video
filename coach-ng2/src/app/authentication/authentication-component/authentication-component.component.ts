import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { ParsemanagerService } from '../../services/parsemanager.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-authentication-component',
  templateUrl: './authentication-component.component.html',
  styleUrls: ['./authentication-component.component.css']
})
export class AuthenticationComponentComponent implements OnInit {
  private user: any;
  username: string = '';
  password: string = '';
  loading = false ;

  constructor(private rutas: Router, private parsemanager: ParsemanagerService) { }

  ngOnInit() {
     console.log("AuthenticationComponentComponent init");
       // let user = this.parsemanager.init();
       // console.log(`user => ${JSON.stringify(user)}`);
       if (this.parsemanager.isAuthenticated()) {
          console.log ("isAuthenticated => true");
          this.rutas.navigate(['home']);
       } else {
          console.log ("isAuthenticated => false");
          this.rutas.navigate(['auth']);
       };


  }

  login(username: string, password: string) {
      this.loading = true;
      this.parsemanager.login(username, password).then(
        user => {
          this.loading = false;
          this.isAuth();
        },
        err => {
          this.loading = false;
          this.isAuth();
        }
      );
      //this.isAuth();
  }

  isAuth(){
       if (this.parsemanager.isAuthenticated()) {
          console.log ("isAuthenticated => true");
          this.rutas.navigate(['home']);
       } else {
          console.log ("isAuthenticated => false");
          this.rutas.navigate(['auth']);
       };
  }

}
