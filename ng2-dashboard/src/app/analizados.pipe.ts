import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'analizados'
})
export class AnalizadosPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    //return null;
    return value.filter(val => val.get('pedidoEstado').get('orden') === 3);
  }

}