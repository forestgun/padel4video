import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationComponentComponent } from './authentication-component/authentication-component.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { SignupComponentComponent } from './signup-component/signup-component.component';


const routes: Routes = [
  {path: 'auth', component: AuthenticationComponentComponent },
  {path: 'auth/login', component: LoginComponentComponent },
  {path: 'auth/signup', component: SignupComponentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class AuthenticationRoutingModule { }
