import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { CurrentUser } from '../shared/CurrentUser';

import { Pedido } from '../shared/pedido.interface';
import { ParsemanagerService } from './parsemanager.service';
import { ParseobjectService  } from './parseobject.service';



//const Parse: any = require('parse');
// PARSE
import * as Parse from 'parse';


@Injectable()
export class ParselivequeryService  {
  private subject = new ReplaySubject<CurrentUser>();

  pedidosquery: any;
  estadoquery: any;
  subscription: any;
  client: any;
  LiveQueryClient: any;
  parseSession: any;
  public modelopedidos: Array<any>  ;
  parseUserCurrent: any;
  //pedido: Pedido;

  constructor(private parsemanager: ParsemanagerService, 
              private parseobject: ParseobjectService
  ) {
    console.log("ParselivequeryService init");
    //Parse.initialize('myAppId_9999', '66b0f6086a032');
    //Parse.serverURL = 'http://parse4video.herokuapp.com/parse';
    //this.subscription = this.newsSubscription()

    this.modelopedidos = [];
    this.parseSession = "";
    console.log(this.parsemanager.currentUser);
    let currentuser = this.parsemanager.currentUser;
    console.log(`currentuser ${JSON.stringify(currentuser)}`);
    if (currentuser) {
      console.log(`  ParselivequeryService currentuser => ${currentuser}  ` );
      console.log(currentuser);
      console.log(currentuser.id);
      //let currentuserId = this.parsemanager.currentUser.id;
      let currentuserId = currentuser.id;
      console.log(`  ParselivequeryService currentuserId => ${currentuserId}  ` );
      //this.parseUserCurrent = this.parseobject.createPointerFromId('User', currentuserId) ;
      //console.log(" this.parseUserCurrent => ");
      //console.log(this.parseUserCurrent);
      if(currentuserId) {
        console.log(`currentuserId ${currentuserId}`);
        this.parseUserCurrent = currentuser.toPointer();
        console.log(this.parseUserCurrent);
        this.parseSession = currentuser.sessionToken;
      }
    }


    //this.parseSession = parsemanager.currentUser.sessionToken;

    console.log("  this.LiveQueryClient = Parse.LiveQueryClient ");
    this.LiveQueryClient = Parse.LiveQueryClient;
    this.client = new this.LiveQueryClient({
      applicationId: 'myAppId_9999',
      serverURL: 'wss://parse4video.herokuapp.com/parse',
      javascriptKey: '66b0f6086a032',
      masterKey:  '' //'myMasterKey_6666'
    });
    this.startControl()
      .subscribe( (result) =>{
        console.log(`startControl => ${JSON.stringify(result)}`)
      });
    console.log("client.open() launch");
    this.client.open();

    this.pedidosquery = new Parse.Query('Pedido');
    this.estadoquery = new Parse.Query('Estado');
    this.estadoquery.lessThan('orden', 4 ); // tenemos hasta estado 3 ponemos 4 para que devuelva TODOS

    this.pedidosquery.include("pedidoUser");
    this.pedidosquery.include("pedidoVideos");
    this.pedidosquery.include("pedidoAnalisis");
    this.pedidosquery.include("pedidoTipo");
    this.pedidosquery.include("pedidoPago");
    this.pedidosquery.include("pedidoEstado");
    this.pedidosquery.ascending("createdAt");
    this.pedidosquery.matchesQuery("pedidoEstado", this.estadoquery); // TODO PENDIENTE DE VERIFICAR SI FILTA POR CURRENT USER
    console.log(`Creamos pedidosquery => ${JSON.stringify(this.pedidosquery)}` );
    this.getPedidos().subscribe( pedidos => {
      this.modelopedidos = pedidos;
      console.log("ParselivequeryService - getpedido().subscribe => this.modelopedidos");
      console.log(this.modelopedidos);
    });
    let newsubscription = this.newsSubscription();
    console.log(`newsubscription => ${JSON.stringify(newsubscription)}`);
  }





  public newsSubscription() {
    // Subscricion a Parse LiveQuery
    console.log(`newsSubscription 1 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
    if (!this.pedidosquery) {
      //this.pedidosquery = new Parse.Query('Pedido');
      //console.log(`newsSubscription 2 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
      //this.loadPedidos();
    }
    // this.pedidosQuery.equalTo('title', 'broadcast');
    console.log(`newsSubscription 3 pedidosquery? => ${JSON.stringify(this.pedidosquery)}` );
    this.subscription = this.client.subscribe(this.pedidosquery, this.parseSession);
    return this.subscription
  }

  public startUpdate(): Observable<any> {
    // Observable para los eventos del Modelo de Datos subscriptos a Parse LiveQuery
    console.log(`call startUpdate() ` );
    return new Observable( observer => {

      // CREATE
      this.subscription.on('create', (pedidos) => {
        console.log(`call this.subscription.on(create) ` );
        let pedido: Pedido =  {
            objectId: pedidos['id'],
            fecha: pedidos.get('createdAt'),
            userpointer: pedidos.get('pedidoUser'),
            videopointer: pedidos.get('pedidoVideo'),
            analisispointer: pedidos.get('pedidoAnalisis'),
            tipopointer: pedidos.get('pedidoTipo'),
            pagado: pedidos.get('pagado'),
            pagopointer: pedidos.get('pedidoPago'),
            estadopointer: pedidos.get('pedidoEstado'),
          observaciones: pedidos.get('Observaciones')};

        /*
        pedido.objectId = pedidos.get('objectId');
        pedido.fecha = pedidos.get('createdAt');
        pedido.userpointer = pedidos.get('pedidoUser');
        pedido.videopointer = pedidos.get('pedidoVideo');
        pedido.analisispointer = pedidos.get('pedidoAnalisis');
        pedido.tipopointer = pedidos.get('pedidoTipo');
        pedido.pagado = pedidos.get('pagado');
        pedido.pagopointer = pedidos.get('pedidoPago');
        pedido.estadopointer = pedidos.get('pedidoEstado');
        pedido.observaciones = pedidos.get('Observaciones');
        */
        console.log(pedido);
        //this.modelopedidos.push(pedido);

        console.log(`PEDIDO create => ${JSON.stringify(pedido)}` );
        observer.next(pedido);
      });

      // UPDATE
      this.subscription.on('update', (pedidos) => {
        console.log(`call this.subscription.on(update) ` );
        console.log(pedidos);
        //let test = this.modelopedidos.filter(pedido => pedido.objectId == pedidos.get("objectId"));
        //console.log(test);

        observer.next(pedidos);
      });

      // CLOSE
      this.subscription.on('close', () => {
        console.log(`call this.subscription.on(close) ` );
        observer.next();
      });

      // DELETE
      this.subscription.on('delete', (pedidos) => {
        console.log(`call this.subscription.on(delete) ` );
        observer.next(pedidos);
      });

      // LEAVE
      this.subscription.on('leave', (pedidos) => {
        console.log(`call this.subscription.on(leave) ` );
        observer.next(pedidos);
      });

      // ENTER
      this.subscription.on('enter', (pedidos) => {
        console.log(`call this.subscription.on(enter) ` );
        observer.next(pedidos);
      });

      // OPEN
      this.subscription.on('open', () => {
        console.log(`call this.subscription.on(open) ` );
        observer.next();
      });

    })
  }

  public startControl(): Observable<string> {
    // Observable para los eventos de control del WebSocket subscriptos a Parse LiveQuery
    console.log(`call startControl() ` );
    return new Observable( observer => {
      this.client.on('open', () => {
        console.log(`call this.client.on(open) ` );
        observer.next('open');
      });
      this.client.on('close', () => {
        console.log(`call this.client.on(close) ` );
        observer.next('close');
      });
      this.client.on('error', () => {
        console.log(`call this.client.on(error) ` );
        observer.next('error');
      });
      // TODO: other events
      // this.subscription.on('update', (news) => {
      //   this.zone.run(()=> {
      //     this.title = news.get('message')
      //   })
      // })
    })
  }

  public stopUpdate() {
    // eliminamos subcricion a Parse LiveQuery
    this.subscription.unsubscribe();
    this.client.close();
  }

  public getModelPedidos(){
    // Pedidos Todos los pedidos que estan en this.modelopedidos con un  Observable
    console.log("getModelPedidos() ");
    return this._getPedidos();
  }

  private _getPedidos(): Observable<any>{
    // Observable de this.modelopedidos
    return new Observable( observer => {
      console.log('_getPedidos');
      console.log(this.modelopedidos);
      observer.next(this.modelopedidos)
    })

  }

  public getPedidos(){
    // Llama a loadPedidos()
    console.log("getPedidos() ");

    return this.loadPedidos();

  }

  public loadPedidos(): Observable<any> {
    // Observable del resultado de una query que devuelve TODOS los Pedidos
    return new Observable( observer => {
      this.pedidosquery.find().then(function (results) {
        console.log('loadPedidos() : results => ' + JSON.stringify(results));
        /*
        console.log(results.length);
        let arraypedidos = [];
        for (let item of results){
          let pedido: Pedido = {
            objectId: item['id'],
            fecha: item.get('createdAt'),
            userpointer: item.get('pedidoUser'),
            videopointer: item.get('pedidoVideo'),
            analisispointer: item.get('pedidoAnalisis'),
            tipopointer: item.get('pedidoTipo'),
            pagado: item.get('pagado'),
            pagopointer: item.get('pedidoPago'),
            estadopointer: item.get('pedidoEstado'),
            observaciones: item.get('Observaciones')
          };
          //console.log(pedido);
          arraypedidos.push(pedido) ;

        }
        //console.log("arraypedidos");
        //console.log(arraypedidos);
        observer.next(arraypedidos);
        */
        observer.next(results);
      }, function (error) {
        console.log('loadPedidos() : error => ' + JSON.stringify(error));
        observer.error(error)

      });
    });

  }

  public getmodelopedido(id: string){
    // devuelve de this.modelopedidos el pedido que coincida con id
    console.log("getmodelopedido");
    //console.log(this.modelopedidos);

    let pedido = this.modelopedidos.filter(val => val['id'] === id);
    //console.log(pedido);
    if (pedido.length > 0){
      return pedido
    } else {
      return []
    }

  }

}
