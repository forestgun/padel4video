import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { CurrentUser } from '../shared/CurrentUser';


//const Parse: any = require('parse');
// PARSE
import * as Parse from 'parse';


@Injectable()
export class ParsemanagerService {
    private subject = new ReplaySubject<CurrentUser>();

    public currentUser: any;


    constructor() {
      console.log("ParsemanagerService init");
        Parse.initialize('myAppId_9999','66b0f6086a032');
        Parse.serverURL= 'https://parse4video.herokuapp.com/parse';
        this.currentUser = {};
        this.init;

    }

    login(username: String, password: String) {
        Parse.User.enableRevocableSession();
        console.log(`username => ${JSON.stringify(username)}`);
        console.log(`password => ${JSON.stringify(password)}`);
        return new Promise((resolve, reject) => {
            resolve(Parse.User.logIn(username, password).then(
            (user) => {
                this.subject.next(this.parseUser2CurrentUser(user));
            },
            (err) => {
                console.error("Error: " + err.code + " " + err.message);
                this.subject.next(null);
            }
        ));
        });
    }

    logout() {
        return new Promise((resolve, reject) => {
            resolve(Parse.User.logOut().then((user) => {
                        console.log("Loggout: ", user);
                        this.subject.next(null);
                    },
                    (err) => {
                            console.error("Error: " + err.code + " " + err.message);
                            this.subject.next(null);
                        }
            ));

        });

    }

    signup(username: String, password: String) {
        console.log(`username => ${JSON.stringify(username)}`);
        console.log(`password => ${JSON.stringify(password)}`);
        return new Promise((resolve, reject) => {
          let user = new Parse.User();
          user.set('username', username);
          user.set('password', password);
          /*
           user.signUp().then(function(){
           success();
           }, function(e){
           console.log('Signin failed through email');
           console.log(` ${JSON.stringify(e)} `);
           error();
           });
           */
          user.signUp().then(
            (success) => {
              console.log(`success: => ${JSON.stringify(success)}`);
              resolve(success);
            },
            (error) => {
              console.error(`Error => ${JSON.stringify(error)}`);
              console.error("Error: " + error.code + " " + error.message);
              //this.subject.next(null);
              reject(error);
            }
          );
        });
    }

    init() {
        console.log('ParsemanagerService.init()');
        console.log(`this.subject => ${JSON.stringify(this.subject)}`);
        this.subject.next(this.getCurrentUser());
    }

    getAuthObsevable(): Observable<CurrentUser>{
        return this.subject.asObservable();
    }

    isAuthenticated() {
        const currentUser = Parse.User.current();
        if (currentUser) {
            return true;
        }
        return false;
    }

    getCurrentUser(): any  {
        console.log('ParsemanagerService.getCurrentUser()');
        const parseUser = Parse.User.current();
        console.log(`parseUser => ${JSON.stringify(parseUser)}`);
        return this.parseUser2CurrentUser(parseUser);
    }

    private parseUser2CurrentUser(parseUser: any){
        console.log('ParsemanagerService.parseUser2CurrentUser()');
        console.log(`parseUser => ${JSON.stringify(parseUser)}`);
        this.currentUser = parseUser;
        console.log(`currentUser => ${JSON.stringify(this.currentUser)}`);
        if (parseUser) {
            return {
              email: parseUser.get("email"),
              username: parseUser.get("username"),
              nickname: parseUser.get("nickname"),
              avatar: parseUser.get("avatar"),
              objectId: parseUser.get('objectId'),
              sessionToken: parseUser.get('sessionToken')
            };

        } else {
            return null;
        }
    }



}
