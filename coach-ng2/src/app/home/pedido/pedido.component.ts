import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import {MdSelectChange} from '@angular/material';

//import { ParselivequeryService } from '../../services/parselivequery.service';
import { ParseobjectService } from '../../services/parseobject.service';
import { Pedido } from '../../shared/pedido.interface';

import * as Parse from 'parse';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

  latestChangeEvent: MdSelectChange;
  pedido: any;
  videos: Array<any> = [];
  vgdash1: string;
  vghls1: string;
  vgmp41: string;
  vgdash2: string;
  vghls2: string;
  vgmp42: string;
  vgdash_analisis: string;
  vghls_analisis: string;
  vgmp4_analisis: string;
  item: {};
  analisis: any;
  estadospedidos: any;
  selectedValue: any;


  constructor( // private parselive: ParselivequeryService,
                private parseobject: ParseobjectService,
                private route: ActivatedRoute,
                private router: Router
                //,
                //private service: HeroService
            )
  {
    console.log("PedidoComponent constructor()");
    //this.pedido = new Parse.Object('Pedido');
    this.pedido = undefined; 
    console.log(this.pedido);

    this.vgdash1 = '';
    this.vghls1 = '';
    this.vgmp41 = '';
    this.vgdash2 = '';
    this.vghls2 = '';
    this.vgmp42 = '';

    this.vgdash_analisis = '';
    this.vghls_analisis = '';
    this.vgmp4_analisis = '';


    let id = this.route.snapshot.params['id'];
    //console.log(id);
    //console.log(this.route.snapshot.params);
    this.parseobject.getPedido(id).then((result) => {
        this.pedido = result;
        this.onPedido();

    }, (error) => {
          console.log("error de getPedido()");
          console.log(error);
    });

    this.estadospedidos = this.parseobject.estadospedidos;
    this.selectedValue = this.estadospedidos[0];
  }

  ngOnInit() {
    console.log("PedidoComponent Inicio");

    

    /*
    this.route.params
    // (+) converts string 'id' to a number
    .switchMap((params: Params) => this.service.getHero(+params['id']))
    .subscribe((hero: Hero) => this.hero = hero);
    */
      // (+) converts string 'id' to a number


  }



  onPedido() {
    console.log(this.pedido);
    let id_estado = this.pedido['attributes']['pedidoEstado']['id'];
    console.log(id_estado);
    for (let _estado of this.estadospedidos){
      console.log(_estado);
      if (id_estado == _estado.id) {
        this.selectedValue = _estado;
      }
    }

    console.log("selectedValue --> ");
    console.log(this.selectedValue);

    console.log(this.pedido);

      let filename1 = this.pedido['attributes']['pedidoVideos'][0]['attributes']['filename'];
      let urldash1 = this.pedido['attributes']['pedidoVideos'][0]['attributes']['url'];
      this.vgdash1 =  urldash1 + filename1 + '-master-playlist-dash.mpd';
      this.vghls1 =  urldash1 + filename1 + '-master-playlist-hls.m3u8';
      this.vgmp41 =  urldash1 + 'mp4-' + filename1 + '.mp4';
      if (this.pedido['attributes']['pedidoVideos'].length >= 2) {
        let filename2 = this.pedido['attributes']['pedidoVideos'][1]['attributes']['filename'];
        let urldash2 = this.pedido['attributes']['pedidoVideos'][1]['attributes']['url'];
        this.vgdash2 =  urldash2 + filename2 + '-master-playlist-dash.mpd';
        this.vghls2 =  urldash2 + filename2 + '-master-playlist-hls.m3u8';
        this.vgmp42 =  urldash2 + 'mp4-' + filename2 + '.mp4';
      } else { 
        // solo tiene subido 1 video

      }

      if (this.pedido['attributes']['pedidoAnalisis']) {
        // El pedido ya tiene una Analisis
        this.analisis = this.pedido['attributes']['pedidoAnalisis'];

        let filenameAnalisis = this.pedido['attributes']['pedidoAnalisis']['attributes']['videoAnalisis'];
        let urldashAnalisis = this.pedido['attributes']['pedidoAnalisis']['attributes']['urlAnalisis'];
        this.vgdash_analisis =  urldashAnalisis + filenameAnalisis + '-master-playlist-dash.mpd';
        this.vghls_analisis =  urldashAnalisis + filenameAnalisis + '-master-playlist-hls.m3u8';
        this.vgmp4_analisis =  urldashAnalisis + filenameAnalisis + '.mp4';

      } else {
        // El pedido no tiene analisis enlazado.


      }
  }

  onChange(value) {
    console.log(value);
    let estado_selected_id = value.value;
    // hacer pointer de estado con el id => estado_selected_id
    let estadopointer = this.parseobject.createPointerFromId("Estado", estado_selected_id);
    console.log(estadopointer);
    // en Pedido asignar el nuevo pointer de estado
    this.pedido.set('pedidoEstado', estadopointer);
    // pedido.save() para actualizar parse-server
    this.pedido.save(); 

  }

  gotoDashboard() {
    this.router.navigate(['dashboard']);
  }

  gotoUpload(id: any) {
    console.log('gotoUpload(id) => id == ');
    console.log(id);
    this.router.navigate(['/home/uploadvideo', id]);
    
  }


}
