var express = require('express');
var app = express();

/*
var aws_s3 = require('aws-sdk');
aws_s3.config.loadFromPath('./config.json');
const S3_BUCKET = process.env.S3_BUCKET;
*/

app.set('port', (process.env.PORT || 5000));

// website test
//=============
var website_test = express.Router();
app.use('/test', website_test);
app.use('/test', express.static("./angular_material_dark/"));
console.log(__dirname + "./angular_material_dark/");

website_test.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});

website_test.get('/test', function(req, res) {
    var path = 'index.html';
    res.sendFile(path, { 'root': './angular_material_dark/' });
});


// website root
//=============
var website = express.Router();
app.use('/', website);
app.use('/', express.static("./templatewire_XOT/"));
console.log(__dirname + "./templatewire_XOT/");

website.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});

website.get('/test', function(req, res) {
    var path = 'index.html';
    res.sendFile(path, { 'root': './templatewire_XOT/' });
});



//dashboard api
//===============
var dashboard = express.Router();
app.use('/app', dashboard);
app.use('/app', express.static("./ng2-dashboard/dist/"));
console.log(__dirname + "/ng2-dashboard/dist/");


/*
dashboard.use(function(req, res, next) {
    console.log('dist.use => ');
    console.log(req.method, req.url);
    next();
});
*/

dashboard.all(/\/app.*/, function(req, res) {
    console.log(' #### app.get => ');
    console.log(req.method, req.url);
    var file = 'index.html';
    res.sendFile(file, { 'root': './ng2-dashboard/dist/' } );

});


//coach api
//===============
var coach = express.Router();
app.use('/coach', coach);
app.use('/coach', express.static("./coach-ng2/dist/"));
console.log(__dirname + "/coach-ng2/dist/");


/*
dashboard.use(function(req, res, next) {
    console.log('dist.use => ');
    console.log(req.method, req.url);
    next();
});
*/

coach.all(/\/coach.*/, function(req, res) {
    console.log(' #### coach.get => ');
    console.log(req.method, req.url);
    var file = 'index.html';
    res.sendFile(file, { 'root': './coach-ng2/dist/' } );

});

// BLOQUE PARA QUE EL CLIENTE PUEDA SUBIR A AWS S3 PASANDO POR NODEJS 
// SIN TENER LA INFO DE AWS-S3 EN EL NAVEGADOR
// ===================================================================
/*
app.get('/sign-s3', (req, res) => {
  const s3 = new aws.S3();
  const fileName = req.query['file-name'];
  const fileType = req.query['file-type'];
  const s3Params = {
    Bucket: S3_BUCKET,
    Key: fileName,
    Expires: 60,
    ContentType: fileType,
    ACL: 'public-read'
  };

  s3.getSignedUrl('putObject', s3Params, (err, data) => {
    if(err){
      console.log(err);
      return res.end();
    }
    const returnData = {
      signedRequest: data,
      url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`
    };
    res.write(JSON.stringify(returnData));
    res.end();
  });
});

app.post('/save-details', (req, res) => {
  // TODO: Read POSTed form data and do something useful
});
*/


// LISTEN FINAL DE LA APP NODEJS
app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});