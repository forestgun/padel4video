import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ParsemanagerService } from '../../services/parsemanager.service';
import { ParselivequeryService } from '../../services/parselivequery.service';

import {Subscription} from "rxjs/Subscription";
import {MediaChange, ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css']
})
export class HomeComponentComponent implements OnInit, OnDestroy {
  loading = false ;
  sub: any;

  watcher: Subscription;
  activeMediaQuery = "";

  mode: string;
  opened: string;

  constructor(private rutas: Router,
              private routeactiva: ActivatedRoute,
              private parsemanager: ParsemanagerService,
              private parselivequery: ParselivequeryService,
              private zone: NgZone,
              private media: ObservableMedia
  ) {
    this.mode = 'over';
    this.opened = 'false';

    if (this.media.isActive('md') || this.media.isActive('lg') || this.media.isActive('xl') ) {
       this.loadDesktopContent();
    }

    this.watcher = media.subscribe((change: MediaChange) => {
      console.log('change: MediaChange ==> ');
      console.log(change);

      this.activeMediaQuery = change ? `'${change.mqAlias}' = (${change.mediaQuery})` : "";
      if ( change.mqAlias == 'md' || change.mqAlias == 'lg' || this.media.isActive('xl')) {
         this.loadDesktopContent();
      } else {
         this.loadMobileContent();
      }
    });

  }

  ngOnInit() {
    console.log("HomeComponentComponent init");
    this.sub = this.routeactiva.data.subscribe(params => {
      //this.id = +params['id']; // (+) converts string 'id' to a number
      console.log(` routeactiva.params => ${JSON.stringify(params)} `);
      console.log(` routeactiva => ${this.routeactiva.toString()} `);
      // In a real app: dispatch action to load the details here.
    });

    this.parselivequery.startUpdate()
      .subscribe( pedido =>{
        //this.zone.run(() => {
        console.log(`HOME pedidos => ${JSON.stringify(pedido)}`);
        //})
      });

     // let user = this.parsemanager.init();
     // console.log(`user => ${JSON.stringify(user)}`);
     if (this.parsemanager.isAuthenticated()) {
        console.log ("isAuthenticated => true");
        this.rutas.navigate(['home'],['dashboard']);
     } else {
        console.log ("isAuthenticated => false");
        this.rutas.navigate(['auth']);
     }
  }

  ngOnDestroy() {
    this.watcher.unsubscribe();
  }

  loadMobileContent() { 
    // Do something special since the viewport is currently
    // using mobile display sizes
    console.log("loadMobileContent()");
    this.mode = 'over';
    this.opened = 'false';
  }

  loadDesktopContent() { 
    // Do something special since the viewport is currently
    // using mobile display sizes
    console.log("loadDesktopContent()");
    this.mode = 'side';
    this.opened = 'true';

  }

  logout(username: string, password: string) {
      this.loading = true;
      this.parsemanager.logout().then(
        user => {
          this.loading = false;
          this.isAuth();
        },
        err => {
          this.loading = false;
          this.isAuth();
        }
      );
      //this.isAuth();
  }

  isAuth(){
       if (this.parsemanager.isAuthenticated()) {
          console.log ("isAuth() = isAuthenticated => true");
          this.rutas.navigate(['home']);
       } else {
          console.log ("isAuth() = isAuthenticated => false");
          this.rutas.navigate(['auth']);
       };
  }

}
