#!/bin/sh

set -e

sed -i.bak "/core-js/d" "./node_modules/videogular2/src/core/vg-media/i-playable.d.ts"
sed -i.bak "/core-js/d" "./node_modules/videogular2/src/core/vg-media/vg-media.d.ts"
sed -i.bak "/core-js/d" "./node_modules/videogular2/src/core/services/vg-api.d.ts"

ng build --prod --aot=false -bh /app -d /app