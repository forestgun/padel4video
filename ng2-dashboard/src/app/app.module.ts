import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MomentModule, DateFormatPipe} from 'angular2-moment';
import 'moment/locale/es';

import { MaterialModule } from '@angular/material';
import 'hammerjs';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AuthenticationModule } from './authentication/authentication.module';
import { HomeModule } from './home/home.module';

import { ParsemanagerService } from './services/parsemanager.service';
import { ParselivequeryService } from './services/parselivequery.service';
import { ParseobjectService } from './services/parseobject.service';
import { AuthGuardService } from './services/auth-guard.service';

import { Router } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MomentModule,
    MaterialModule,
    FlexLayoutModule,
    AuthenticationModule,
    HomeModule
  ],
  providers: [ParsemanagerService, ParselivequeryService, ParseobjectService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {

  // Diagnostic only: inspect router configuration
  constructor(router: Router) {
    console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }

}
