import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ParsemanagerService } from './services/parsemanager.service';



// PARSE
import * as Parse from 'parse';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  constructor(private rutas: Router, private parsemanager: ParsemanagerService) {
    console.log(`AppModule.constructor ==> `);
    /*
    // initialize( applicationId, javaScriptKey, masterKey )
    console.log(`AppModule.constructor ==> Parse.initialize()`);
    Parse.initialize('myAppId_9999','66b0f6086a032' );
    console.log(`AppModule.constructor ==> Parse.serverURL()`);
    Parse.serverURL = 'http://parse4video.herokuapp.com/parse';
    console.log(`AppModule.constructor ==> Parse.User()`);
    Parse.User.enableRevocableSession();
    let current = Parse.User.current();
    console.log(`AppModule.constructor ==> user = ${JSON.stringify(current)}`);

    let username = 'fortes.movil@gmail.com';
    let password = 'celtarra';

    if (!current) {
      this.loginosignup(username, password);
      console.log(`AppModule.constructor ==> !CURRENT => user = ${JSON.stringify(current)}`);
      let islogin = Parse.User.current();
      if (islogin) {
        console.log(`AppModule.constructor => islogin=true => go to /home `);
        this.rutas.navigate(['home']);
      } else {
        console.log(`AppModule.constructor => islogin=false => go to /auth `);
        this.rutas.navigate(['auth']);
      }

    } else {
      let sessionToken = current.get('sessionToken');
      console.log(`AppModule.constructor ==> sessionToken = ${JSON.stringify(sessionToken)}`);
      Parse.User.become(sessionToken).then( function (user) {
        // The current user is now set to user
        console.log(`AppModule.constructor ==> The current user is now set to user`);
        console.log(`AppModule.constructor ==> user = ${JSON.stringify(user)}`);
        this.rutas.navigate(['home'], {relativeTo: rutas});
      }, function (error) {
        // The token could not be validated
        console.log(`AppModule.constructor ==> The token could not be validated`);
        console.log(`AppModule.constructor ==> error = ${JSON.stringify(error)}`);
        this.rutas.navigate(['auth'], {relativeTo: rutas});
      });
      console.log(`AppModule.constructor ==> user = ${JSON.stringify(current)}`);
    }
    */
  }

    ngOnInit() {
        console.log("appComponent init");
        let user = this.parsemanager.init();
        console.log(`user => ${JSON.stringify(user)}`);
        if (!user) {
           this.rutas.navigate(['auth']);
        }
    }

// METODOS

  loginosignup(username: String, password: String) {
      this.logIn(username, password, function(){
        console.log('User logged in through email');
        // self.router.parent.navigate('/home');
      }, function(){
        // self.errorMsg = 'Wrong user/pass';
        console.log('Wrong user/pass');
        this.signup(username, password, ()=>{
          console.log('User signed in through email');
          // self.router.parent.navigate('/home');
        });
      });

  }

  logIn(username: String, password: String, success: () =>  void , error: () => void) {
    Parse.User.logIn(username, password).then(function(){
      console.log('login success()');
      success();
    },function(e){
      console.log('login error()');
      console.log(`${JSON.stringify(e)}`);
      error();
    });
  }

  signup(username: String, password: String, success: () => void , error: () => void ) {
    let user = new Parse.User();
    user.set('username', username);
    user.set('password', password);

    user.signUp().then(function(){
      success();
    }, function(e){
      console.log('Signin failed through email');
      console.log(` ${JSON.stringify(e)} `);
      error();
    });
  }


}
